const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const userRouter = require('./routes/user')
const app = express()

const PORT = 3000

mongoose.connect('mongodb://localhost/jwtauth')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.get('/checking', (req, res) => {
  res.json({ message: 'Test' })
})

app.use('/user', userRouter)

app.listen(PORT, () => {
  console.log(`I'm listening to ${PORT}`)
})
